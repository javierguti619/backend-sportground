package com.gutierrez.sportGround.dao.repository;

import com.gutierrez.sportGround.dao.entity.ReservationEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

public interface ReservationRepository extends JpaRepository<ReservationEntity, Serializable> {

    ReservationEntity findById(int id);

    List<ReservationEntity> findByUserId(int userId);

    List<ReservationEntity> findBySportGroundId(int groundId);

    List<ReservationEntity> findByReservationDateAndSportGroundId(LocalDate reservationDate, int sportGroundId);

}
