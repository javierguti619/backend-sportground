package com.gutierrez.sportGround.controller;


import com.gutierrez.sportGround.dao.entity.GroundEntity;
import com.gutierrez.sportGround.model.MGround;
import com.gutierrez.sportGround.service.GroundService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
@RequestMapping("/ground")
public class GroundController {

    @Autowired
    @Qualifier("groundService")
    private GroundService groundService;

    @PutMapping("/add")
    public boolean addGround(@RequestBody @Valid GroundEntity ground) {

        return groundService.create(ground);
    }

    @GetMapping("/all")
    public List<MGround> getAllGrounts() {return groundService.getsAll();}

    @GetMapping("/{id}")
    public MGround getGround(@PathVariable("id") int id) {return groundService.getsGround(id);}

}
