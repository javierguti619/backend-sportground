package com.gutierrez.sportGround.dao.repository;

import com.gutierrez.sportGround.dao.entity.GroundEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.List;

@Repository("groundRepository")
public interface GroundRepository extends JpaRepository<GroundEntity, Serializable> {

    GroundEntity findById(int id);

    List<GroundEntity> findByVenueId(int id);

    GroundEntity getById(int id);

}
