package com.gutierrez.sportGround.model;


import com.gutierrez.sportGround.dao.entity.UserEntity;
import lombok.Data;

@Data
public class MUser {

    public MUser() {

    }

    public MUser(UserEntity user) {
        this.id = user.getId();
        this.name = user.getName();
        this.document = user.getDocument();
        this.email = user.getEmail();
        this.password = null;
        this.phone = user.getPhone();
        this.userType = user.getUserType();
    }

    public MUser(int id, String name, String document, String email, String password, String phone, String userType) {
        this.id = id;
        this.name = name;
        this.document = document;
        this.email = email;
        this.password = null;
        this.phone = phone;
        this.userType = userType;
    }

    private int id;
    private String name;
    private String document;
    private String email;
    private String password;
    private String phone;
    private String userType;

}
