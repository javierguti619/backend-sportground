package com.gutierrez.sportGround.controller.request;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class UpdatePermissionRequest {
    private Integer userId;
    private String permission;
}
