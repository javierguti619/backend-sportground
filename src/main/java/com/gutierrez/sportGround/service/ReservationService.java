package com.gutierrez.sportGround.service;

import com.gutierrez.sportGround.converter.ReservationConverter;
import com.gutierrez.sportGround.dao.entity.ReservationEntity;
import com.gutierrez.sportGround.mail.EmailService;
import com.gutierrez.sportGround.mail.EmailType;
import com.gutierrez.sportGround.model.MReservation;
import com.gutierrez.sportGround.dao.repository.ReservationRepository;
import com.gutierrez.sportGround.model.MUser;
import com.gutierrez.sportGround.type.DeportType;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequiredArgsConstructor
@Service
public class ReservationService {

    private final ReservationRepository reservationRepository;
    private final ReservationConverter reservationConverter;
    private final EmailService emailService;

    public boolean create(ReservationEntity reservation, MUser mUser) {
        reservation.setUserId(mUser.getId());
        try {
            ReservationEntity save = reservationRepository.save(reservation);
        } catch (Exception e) {
            return false;
        }
        Map<String, String> map = new HashMap<>();
        map.put("name", mUser.getName());
        map.put("ground", DeportType.fromCode(reservation.getDeport()).getDescription());
        map.put("date", reservation.getCreateDate().format(DateTimeFormatter.ofPattern("dd MM")));

        emailService.prepareAndSend(mUser.getEmail(), "Confirmación de reserva", map, EmailType.RESERVATION);
        return true;

    }

    public boolean upDate(ReservationEntity reservation) {

        try {

            reservationRepository.save(reservation);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean delete(int id) {

        try {

            ReservationEntity reservation = reservationRepository.findById(id);
            reservationRepository.delete(reservation);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public List<MReservation> getsAll() {
        return reservationConverter.converList(reservationRepository.findAll());
    }

    public MReservation GetsVenue(int id) {
        return new MReservation(reservationRepository.findById(id));
    }

    public List<MReservation> GetsReservationGround(LocalDate reservation, int groundId) {
        return reservationConverter.converList(reservationRepository.findByReservationDateAndSportGroundId(reservation, groundId));
    }

    public List<MReservation> GetsUserId(int userId) {
        return reservationConverter.converList(reservationRepository.findByUserId(userId));
    }

    public List<MReservation> GetsGrounds(int groundId) {
        return reservationConverter.converList(reservationRepository.findBySportGroundId(groundId));
    }


}
