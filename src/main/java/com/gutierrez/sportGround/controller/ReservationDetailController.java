package com.gutierrez.sportGround.controller;

import com.gutierrez.sportGround.model.MReservationDetail;
import com.gutierrez.sportGround.service.ReservationDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
@RequestMapping("/detail")
public class ReservationDetailController {

    @Autowired
    @Qualifier("reservationDetailService")
    private ReservationDetailService reservationDetailService;

    @GetMapping(value="/id/{id}")
    public List<MReservationDetail> GetsDetail(@PathVariable("id") int id){
        return reservationDetailService.GetsDetail(id);
    }


}
