package com.gutierrez.sportGround.dao.entity;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
public class ReservationDetailEntity implements Serializable {


    public ReservationDetailEntity () {}

    public ReservationDetailEntity(int reserId, LocalDate createDate, LocalDateTime startTime, LocalDateTime endTime, LocalDate reservationDate, String description, String image, String type, String venueName, String address, String phone) {
        this.reserId= reserId;
        this.createDate = createDate;
        this.startTime = startTime;
        this.endTime = endTime;
        this.reservationDate = reservationDate;
        this.description = description;
        this.image = image;
        this.type = type;
        this.venueName = venueName;
        this.address = address;
        this.phone = phone;
    }

   @Id
   @Column(name="reser_id")
   private int reserId;

    @Column(name="create_date")
    private LocalDate createDate;

    @Column(name="start_time")
    private LocalDateTime startTime ;

    @Column(name="end_time")
    private LocalDateTime endTime;

    @Column(name = "reservation_date")
    private LocalDate reservationDate;

    @Column(name="description")
    private String description;

    @Column(name="image")
    private String image ;

    @Column(name="type")
    private String type;

    @Column(name="venue_name")
    private String venueName;

    @Column(name="address")
    private String address ;

    @Column(name="phone")
    private String phone;


    public LocalDate getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public LocalDate getReservationDate() {
        return reservationDate;
    }

    public void setReservationDate(LocalDate reservationDate) {
        this.reservationDate = reservationDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getVenueName() {
        return venueName;
    }

    public void setVenueName(String venueName) {
        this.venueName = venueName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getReserId() {
        return reserId;
    }

    public void setReserId(int reserId) {
        this.reserId = reserId;
    }
}
