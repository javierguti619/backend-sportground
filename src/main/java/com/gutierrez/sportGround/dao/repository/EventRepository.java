package com.gutierrez.sportGround.dao.repository;

import com.gutierrez.sportGround.dao.entity.EventEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Repository("eventRepository")
public interface EventRepository extends JpaRepository<EventEntity, Serializable> {

    EventEntity findByEventId(int id);

    List<EventEntity> findByDateTime(LocalDateTime time);
}
