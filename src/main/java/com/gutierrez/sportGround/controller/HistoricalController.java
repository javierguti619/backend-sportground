package com.gutierrez.sportGround.controller;

import com.gutierrez.sportGround.controller.annotations.FindUser;
import com.gutierrez.sportGround.controller.request.ReservationHistoricalRequest;
import com.gutierrez.sportGround.dao.entity.ReservationHistorical;
import com.gutierrez.sportGround.dao.repository.ReservationHistoricalRepository;
import com.gutierrez.sportGround.model.MUser;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT})
@RequestMapping("/historical")
@NoArgsConstructor
public class HistoricalController {


    @Autowired
    @Qualifier("reservationHistoricalRepository")
    private ReservationHistoricalRepository reservationHistoricalRepository;


    @PostMapping("/reservation")
    public List<ReservationHistorical> getAllReservation(@RequestBody ReservationHistoricalRequest request) {
        return reservationHistoricalRepository.getHistorical(request.getVenueId(), request.getStartDate(), request.getEndDate());
    }

    @PostMapping("/reservationToUser")
    public List<ReservationHistorical> reservationToUser(@FindUser MUser mUser) {
        return reservationHistoricalRepository.getHistoricalByUser(mUser.getId());

    }

}
