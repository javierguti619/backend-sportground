package com.gutierrez.sportGround.dao.repository;


import com.gutierrez.sportGround.dao.entity.ReservationHistorical;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

@Repository
public interface ReservationHistoricalRepository extends JpaRepository<ReservationHistorical, Serializable> {
    @Query(value = "select  r.reser_id, u.user_name , v. venue_name, g.description ground_name, r.reservation_date " +
            "from reservation  r " +
            "left join user  u on r.user_id = u.user_id " +
            "left join ground g on r.sport_ground_id = g.ground_id " +
            "left join  venue v on  g.venue_id = v.venue_id " +
            "where v.venue_id=:venueId " +
            "and (r.reservation_date between :startDate AND :endDate)", nativeQuery = true)
    List<ReservationHistorical> getHistorical(@Param("venueId") Long venueId, @Param("startDate") LocalDate startDate, @Param("endDate") LocalDate endDate);

    @Query(value = "select  r.reser_id, u.user_name , v. venue_name, g.description ground_name, r.reservation_date, r.start_time " +
            "from reservation  r " +
            "left join user  u on r.user_id = u.user_id " +
            "left join ground g on r.sport_ground_id = g.ground_id " +
            "left join  venue v on  g.venue_id = v.venue_id " +
            "where u.user_id=:userId ", nativeQuery = true)
    List<ReservationHistorical> getHistoricalByUser(@Param("userId") int userId);

}
