package com.gutierrez.sportGround.service;

import com.gutierrez.sportGround.converter.VenueConverter;
import com.gutierrez.sportGround.dao.entity.VenueEntity;
import com.gutierrez.sportGround.model.MVenue;
import com.gutierrez.sportGround.dao.repository.VenueRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("venueService")
public class VenueService  {

    @Autowired
    @Qualifier("venueRepository")
    private VenueRepository venueRepository;

    @Autowired
    @Qualifier("venueConverter")
    private VenueConverter venueConverter;

    public boolean create(VenueEntity venue) {

        try{

            venueRepository.save(venue);
            return true;
        } catch (Exception e){
            return false;
        }
    }

    public boolean upDate(VenueEntity venue) {

        try {

            venueRepository.save(venue);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean delete( int id) {

        try {

            VenueEntity venue = venueRepository.findById(id);
            venueRepository.delete(venue);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public List<MVenue> getsAll() {
        return venueConverter.converList(venueRepository.findAll());
    }

    public MVenue getsVenue(int id) {
        return new MVenue(venueRepository.findById(id));
    }

}
