package com.gutierrez.sportGround.controller;

import com.gutierrez.sportGround.controller.annotations.FindUser;
import com.gutierrez.sportGround.controller.request.UpdatePermissionRequest;
import com.gutierrez.sportGround.dao.entity.UserEntity;
import com.gutierrez.sportGround.model.MUser;
import com.gutierrez.sportGround.service.UserService;
import org.hibernate.sql.Update;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT})
@RequestMapping("/user")
public class UserController {

    @Autowired
    @Qualifier("userService")
    UserService service;

    @PutMapping("/add")
    public boolean addUser(@RequestBody @Valid UserEntity user) {
        return service.create(user);
    }

    @GetMapping(value = "/all")
    public List<MUser> getAllUser() {
        return service.gets();
    }

    @GetMapping(value = "/me")
    public MUser getUser(final @FindUser MUser mUser) {
        return service.getUser(mUser.getId());
    }

    @PostMapping(value = "/updatePermission")
    public Boolean updatePermission(@RequestBody UpdatePermissionRequest updatePermission) {
        return service.updatePermission(updatePermission.getPermission(), updatePermission.getUserId());
    }

}
