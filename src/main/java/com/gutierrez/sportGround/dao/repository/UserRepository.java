package com.gutierrez.sportGround.dao.repository;

import com.gutierrez.sportGround.dao.entity.ReservationHistorical;
import com.gutierrez.sportGround.dao.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

@Repository("userRepository")
public interface UserRepository extends JpaRepository<UserEntity, Serializable> {


    UserEntity findByName(String name);

    UserEntity findById(int id);

    UserEntity findByEmail(String email);

    @Modifying
    @Transactional
    @Query(value = "UPDATE user SET user_type=:permission WHERE user_id=:userId", nativeQuery = true)
    void updatePermission(@Param("permission") String permission, @Param("userId") Integer userId);

}
