package com.gutierrez.sportGround;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SportGroundApplication {

	public static void main(String[] args) {
		SpringApplication.run(SportGroundApplication.class, args);
	}

}
