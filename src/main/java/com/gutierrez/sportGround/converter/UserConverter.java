package com.gutierrez.sportGround.converter;


import com.gutierrez.sportGround.dao.entity.UserEntity;
import com.gutierrez.sportGround.model.MUser;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component("converter")
public class UserConverter {

    public List<MUser> converList(List<UserEntity> users){
        List<MUser> mUsers = new ArrayList<>();

        for(UserEntity user: users) {
            mUsers.add(new MUser(user));
        }
        return mUsers;
    }
}
