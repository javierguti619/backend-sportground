package com.gutierrez.sportGround.service;


import com.gutierrez.sportGround.converter.GroundConverter;
import com.gutierrez.sportGround.dao.entity.GroundEntity;
import com.gutierrez.sportGround.model.MGround;
import com.gutierrez.sportGround.dao.repository.GroundRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("groundService")
public class GroundService {

    @Autowired
    @Qualifier("groundRepository")
    private GroundRepository groundRepository;

    @Autowired
    @Qualifier("groundConverter")
    private GroundConverter groundConverter;
/*
    @Autowired
    @Qualifier("reservationRepository")
    private ReservationRepository reservationRepository;

    @Autowired
    @Qualifier("reservationDetailRepository")
    private ReservationDetailRepository reservationDetailRepository;*/

    public boolean create(GroundEntity ground) {

        try {

            groundRepository.save(ground);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean upDate(GroundEntity ground) {

        try {

            groundRepository.save(ground);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean delete( int id) {

        try {

            GroundEntity ground = groundRepository.findById(id);
            groundRepository.delete(ground);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public List<MGround> getsAll() {
        return groundConverter.converList(groundRepository.findAll());
    }

    public MGround getsGround(int id) {
        return new MGround(groundRepository.findById(id));
    }


}

