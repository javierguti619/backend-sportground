package com.gutierrez.sportGround.dao.entity;

import lombok.Builder;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDate;

@Data
@Entity
public class ReservationHistorical {
    @Id
    @Column(name = "reser_id")
    private String reserId;

    @Column(name = "user_name")
    private String userName;

    @Column(name = "venue_name")
    private String venueName;

    @Column(name = "ground_name")
    private String groundName;

    @Column(name = "reservation_date")
    private String reservationDate;

    @Column(name = "start_time")
    private String startTime;
}