package com.gutierrez.sportGround.dao.entity;

import java.io.Serializable;
import javax.persistence.*;

@Table(name="ground")
@Entity
public class GroundEntity implements Serializable {

    public GroundEntity() {
    }

    public GroundEntity(int id, String name, String image, String type, int venueId) {
        this.id = id;
        this.name = name;
        this.image = image;
        this.type = type;
        this.venueId = venueId;
    }

    @Id
    @Column(name = "ground_id")
    private int id;

    @Column(name = "description")
    private String name;

    @Column(name = "image ")
    private String image;


    @Column(name = "type")
    private String type;

    @Column(name = "venue_id")
    private int venueId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getVenueId() {
        return venueId;
    }

    public void setVenueId(int venueId) {
        this.venueId = venueId;
    }
}



