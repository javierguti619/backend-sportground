package com.gutierrez.sportGround.service;

import com.gutierrez.sportGround.converter.ReservationDetailConverter;
import com.gutierrez.sportGround.model.MReservationDetail;
import com.gutierrez.sportGround.dao.repository.ReservationDetailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("reservationDetailService")
public class ReservationDetailService {

    @Autowired
    @Qualifier("reservationDetailRepository")
    private ReservationDetailRepository reservationDetailRepository;

    @Autowired
    @Qualifier("reservationDetailConverter")
    private ReservationDetailConverter reservationDetailConverter;

    public List<MReservationDetail> GetsDetail (int userId) {
        return reservationDetailConverter.converList(reservationDetailRepository.findByUser(userId));
    }

}
