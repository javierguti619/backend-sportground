package com.gutierrez.sportGround.mail;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;

import java.util.Map;

@Service
@RequiredArgsConstructor
public class MailContentBuilder {

    private final TemplateEngine templateEngine;

    public String buildReservationMail(Map<String, String > map, String templateName){

        Context context = new Context();
        map.forEach((k,v)->{
            context.setVariable(k, v);
        });
        final ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
        templateResolver.setTemplateMode(TemplateMode.HTML);

       // templateEngine.setTemplateResolver(templateResolver);
        return templateEngine.process(templateName,context);


    }

}
