package com.gutierrez.sportGround.controller;

import com.gutierrez.sportGround.controller.request.RegisterRequest;
import com.gutierrez.sportGround.dao.entity.UserEntity;
import com.gutierrez.sportGround.model.MGround;
import com.gutierrez.sportGround.model.MVenue;
import com.gutierrez.sportGround.service.GroundService;
import com.gutierrez.sportGround.service.UserService;
import com.gutierrez.sportGround.service.VenueService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
@RequestMapping("/public")
@AllArgsConstructor
public class PublicController {

    private GroundService groundService;
    private VenueService venueService;
    private UserService userService;

    @GetMapping("ground/all")
    public List<MGround> getAllGrounts() {return groundService.getsAll();}

    @GetMapping("venue/all")
    public List<MVenue> getsAll() { return  venueService.getsAll();}

    @PostMapping("user/register")
    public Boolean register(@RequestBody UserEntity userEntity) {
        return  userService.create(userEntity);
    }

}
