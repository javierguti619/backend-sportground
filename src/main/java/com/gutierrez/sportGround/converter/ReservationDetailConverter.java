package com.gutierrez.sportGround.converter;


import com.gutierrez.sportGround.dao.entity.ReservationDetailEntity;
import com.gutierrez.sportGround.model.MReservationDetail;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component("reservationDetailConverter")
public class ReservationDetailConverter {

    public List<MReservationDetail> converList(List<ReservationDetailEntity> reservationDetailEntity){
        List<MReservationDetail> mReservationDetails = new ArrayList<>();

        for(ReservationDetailEntity reservation: reservationDetailEntity) {
            mReservationDetails.add(new MReservationDetail(reservation));
        }
        return mReservationDetails;
    }
}
