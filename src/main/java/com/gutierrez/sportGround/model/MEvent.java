package com.gutierrez.sportGround.model;

import com.gutierrez.sportGround.dao.entity.EventEntity;

import java.time.LocalDateTime;

public class MEvent {

    public MEvent() {}

    public MEvent(EventEntity event) {
        this.eventId = event.getEventId();
        this.venueId = event.getVenueId();
        this.name = event.getName();
        this.groundId = event.getGroundId();
        this.dateTime = event.getDateTime();
        this.status = event.getStatus();
    }

    public MEvent(int eventId,
                  int venueId,
                  String name,
                  int groundId,
                  LocalDateTime dateTime,
                  int status) {
        this.eventId = eventId;
        this.venueId = venueId;
        this.name = name;
        this.groundId = groundId;
        this.dateTime = dateTime;
        this.status = status;
    }

    private int eventId;

    private int venueId;

    private String name;

    private int groundId;

    private LocalDateTime dateTime;

    private int status;

    public int getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    public int getVenueId() {
        return venueId;
    }

    public void setVenueId(int venueId) {
        this.venueId = venueId;
    }

    public int getGroundId() {
        return groundId;
    }

    public void setGroundId(int groundId) {
        this.groundId = groundId;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
