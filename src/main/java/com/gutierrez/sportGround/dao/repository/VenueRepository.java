package com.gutierrez.sportGround.dao.repository;

import com.gutierrez.sportGround.dao.entity.VenueEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;


@Repository("venueRepository")
public interface VenueRepository extends JpaRepository<VenueEntity, Serializable> {

    VenueEntity findById(int id);
}
