package com.gutierrez.sportGround.model;

import com.gutierrez.sportGround.dao.entity.GroundEntity;



public class MGround {

    public MGround (){}

    public MGround(GroundEntity ground) {
        this.id = ground.getId();
        this.name = ground.getName();
        this.image = ground.getImage();
        this.type = ground.getType();
        this.venueId = ground.getVenueId();
    }

    public MGround(int id, String name, String image, String type, int venueId) {
        this.id = id;
        this.name = name;
        this.image = image;
        this.type = type;
        this.venueId = venueId;
    }

    private int id;

    private String name;

    private String image ;

    private String type;

    private int venueId;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getVenueId() {
        return venueId;
    }

    public void setVenueId(int venueId) {
        this.venueId = venueId;
    }
}
