package com.gutierrez.sportGround.controller;


import com.gutierrez.sportGround.dao.entity.VenueEntity;
import com.gutierrez.sportGround.model.MVenue;
import com.gutierrez.sportGround.service.VenueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
@RequestMapping("/venue")
public class VenueController {

    @Autowired
    @Qualifier("venueService")
    private VenueService venueService;


    @PutMapping("/add")
    public  boolean addVenue(@RequestBody @Valid VenueEntity venue) {
        return venueService.create(venue);
    }

    @GetMapping("/all")
    public List<MVenue> getsAll() { return  venueService.getsAll();}

    @GetMapping(value = "/id/{id}")
    public MVenue getsVenue(@PathVariable("id") int id) {
        return venueService.getsVenue(id);
    }

}


