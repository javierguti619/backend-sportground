package com.gutierrez.sportGround.service;


import com.gutierrez.sportGround.converter.UserConverter;
import com.gutierrez.sportGround.dao.entity.UserEntity;
import com.gutierrez.sportGround.mail.EmailService;
import com.gutierrez.sportGround.mail.EmailType;
import com.gutierrez.sportGround.model.MUser;
import com.gutierrez.sportGround.dao.repository.UserRepository;
import com.gutierrez.sportGround.type.DeportType;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
/*import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;*/
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.thymeleaf.util.ObjectUtils;

import javax.transaction.Transactional;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Service("userService")
@AllArgsConstructor
public class UserService implements UserDetailsService {

    private UserRepository userRepository;
    private UserConverter converter;
    private final EmailService emailService;


    public boolean create(UserEntity user) {
        try {
            UserEntity byEmail = userRepository.findByEmail(user.getEmail());
            if (Objects.nonNull(byEmail)) {
                return false;
            }
            userRepository.save(user);
        } catch (Exception e) {
            return false;
        }

        Map<String, String> map = new HashMap<>();
        map.put("name", user.getName());

        emailService.prepareAndSend(user.getEmail(), "Confirmación de registro", map, EmailType.REGISTER);
        return true;
    }

    public boolean upDate(UserEntity user) {
        try {

            userRepository.save(user);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean delete(int id) {
        try {

            UserEntity user = userRepository.findById(id);
            userRepository.delete(user);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public UserEntity loadUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public List<MUser> gets() {
        return converter.converList(userRepository.findAll());
    }

    public MUser getUser(int id) {
        return new MUser(userRepository.findById(id));
    }

    public boolean updatePermission(String permission, Integer userId) {
        userRepository.updatePermission(permission, userId);
        return true;
    }


    //Login

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        UserEntity user = userRepository.findByEmail(email);

        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(!StringUtils.isEmpty(user.getUserType()) ? user.getUserType() : "NO_DEFINIDO"));
        return (UserDetails) new org.springframework.security.core.userdetails.User(String.valueOf(user.getId()), user.getPassword(), authorities);

    }


}
