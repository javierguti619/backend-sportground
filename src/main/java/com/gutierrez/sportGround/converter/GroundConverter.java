package com.gutierrez.sportGround.converter;

import com.gutierrez.sportGround.dao.entity.GroundEntity;
import com.gutierrez.sportGround.model.MGround;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component("groundConverter")
public class GroundConverter {

    public List<MGround> converList(List<GroundEntity> grounds) {
        List<MGround> mGrounds =new ArrayList<>();

        for(GroundEntity ground: grounds){
            mGrounds.add( new MGround(ground));
        }
        return mGrounds;
    }
}
