package com.gutierrez.sportGround.controller;

import com.gutierrez.sportGround.controller.annotations.FindUser;
import com.gutierrez.sportGround.controller.request.DeleteRequest;
import com.gutierrez.sportGround.controller.request.ReservationRequest;
import com.gutierrez.sportGround.dao.entity.ReservationEntity;
import com.gutierrez.sportGround.model.MReservation;
import com.gutierrez.sportGround.model.MUser;
import com.gutierrez.sportGround.service.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST, RequestMethod.PUT})
@RequestMapping("/reservation")
public class ReservationController {

    @Autowired
    @Qualifier("reservationService")
    private ReservationService reservationService;


    @PostMapping("/add")
    public boolean addReserva(@RequestBody @Valid ReservationEntity reserva, final @FindUser MUser mUser) {
        return reservationService.create(reserva, mUser);
    }

    @PostMapping("/delete")
    public boolean delete(@RequestBody DeleteRequest deleteRequest) {
        return reservationService.delete(deleteRequest.getId());
    }

    @GetMapping("/all")
    public List<MReservation> getAll() {return  reservationService.getsAll();}


    @GetMapping(value="/list/{reserva}/{idGround}")
    public List<MReservation> GetsReservationGround(@PathVariable("reserva") LocalDate reserva, @PathVariable("idGround") int idGround ) {
        return  reservationService.GetsReservationGround(reserva, idGround);}


    @GetMapping(value="/user/{id}")
    public List<MReservation> GetsUserId(@PathVariable("id") int id ) {
        return  reservationService.GetsUserId(id);}

    @GetMapping(value="/grounds/{id}")
    public List<MReservation> GetsGrounds(@PathVariable("id") int id ) {
        return  reservationService.GetsGrounds(id);}

    @PostMapping("/reservationByDate")
    public List<MReservation> reservationByDate(@RequestBody ReservationRequest reservationRequest){

        return reservationService.GetsReservationGround(reservationRequest.getReservationDate(), reservationRequest.getSportGroundId());

    }

}
