package com.gutierrez.sportGround.model;

import com.gutierrez.sportGround.dao.entity.ReservationDetailEntity;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class MReservationDetail {

    public MReservationDetail(){

    }

    public MReservationDetail(ReservationDetailEntity reservation){

        this.reserId = reservation.getReserId();
        this.createDate = reservation.getCreateDate();
        this.startTime = reservation.getStartTime();
        this.endTime = reservation.getEndTime();
        this.reservationDate = reservation.getReservationDate();
        this.description = reservation.getDescription();
        this.image = reservation.getImage();
        this.type = reservation.getType();
        this.venueName = reservation.getVenueName();
        this.address = reservation.getAddress();
        this.phone = reservation.getPhone();
    }

    public MReservationDetail(int reserId, LocalDate createDate, LocalDateTime startTime, LocalDateTime endTime, LocalDate reservationDate, String description, String image, String type, String venueName, String address, String phone) {
        this.reserId = reserId;
        this.createDate = createDate;
        this.startTime = startTime;
        this.endTime = endTime;
        this.reservationDate = reservationDate;
        this.description = description;
        this.image = image;
        this.type = type;
        this.venueName = venueName;
        this.address = address;
        this.phone = phone;
    }

    private int reserId;

    private LocalDate createDate;

    private LocalDateTime startTime ;

    private LocalDateTime endTime;

    private LocalDate reservationDate;

    private String description;

    private String image ;

    private String type;

    private String venueName;

    private String address ;

    private String phone;


    public int getReserId() {
        return reserId;
    }

    public void setReserId(int reserId) {
        this.reserId = reserId;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public LocalDate getReservationDate() {
        return reservationDate;
    }

    public void setReservationDate(LocalDate reservationDate) {
        this.reservationDate = reservationDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getVenueName() {
        return venueName;
    }

    public void setVenueName(String venueName) {
        this.venueName = venueName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
