package com.gutierrez.sportGround.dao.entity;

import javax.persistence.*;
import java.io.Serializable;

@Table(name="venue")
@Entity
public class VenueEntity implements Serializable {

    public VenueEntity() {}

    public VenueEntity(int id, String name, String address, String phone) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.phone = phone;
    }

    @Id
    @Column(name="venue_id")
    private int id;

    @Column(name="venue_name")
    private String name;

    @Column(name="address ")
    private String address ;


    @Column(name="phone")
    private String phone;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

}
