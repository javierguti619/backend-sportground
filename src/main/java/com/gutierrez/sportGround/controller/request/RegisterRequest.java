package com.gutierrez.sportGround.controller.request;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class RegisterRequest {
    private String id;
    private String name;
    private String email;
    private String password;
    private String phone;
}
