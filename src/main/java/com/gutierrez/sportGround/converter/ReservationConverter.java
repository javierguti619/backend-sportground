package com.gutierrez.sportGround.converter;


import com.gutierrez.sportGround.dao.entity.ReservationEntity;
import com.gutierrez.sportGround.model.MReservation;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component("reservationConverter")
public class ReservationConverter {

    public List<MReservation> converList(List<ReservationEntity> reservationEntities) {
        List<MReservation> mReservations = new ArrayList<>();

        for(ReservationEntity reservationEntity:reservationEntities) {
            mReservations.add(new MReservation(reservationEntity));
        }
        return mReservations;
    }
}


