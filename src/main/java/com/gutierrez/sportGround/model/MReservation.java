package com.gutierrez.sportGround.model;

import com.gutierrez.sportGround.dao.entity.ReservationEntity;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
public class MReservation {


    public MReservation(){}

    public MReservation(ReservationEntity reservationEntity) {
        this.id= reservationEntity.getId();
        this.createDate = String.valueOf(reservationEntity.getCreateDate());
        this.startTime = String.valueOf(reservationEntity.getStartTime());
        this.endTime = String.valueOf(reservationEntity.getEndTime());
        this.reservationDate = String.valueOf(reservationEntity.getReservationDate());
        this.userId = reservationEntity.getUserId();
        this.deport = reservationEntity.getDeport();
        this.sportGroundId = reservationEntity.getSportGroundId();
        this.pending= reservationEntity.getPending();
    }


    public MReservation(int id, LocalDate createDate, LocalDateTime startTime, LocalDateTime endTime, LocalDate reservationDate, int userId, String deport, int sportGroundId, int pending) {
        this.id = id;
        this.createDate = String.valueOf(createDate);
        this.startTime = String.valueOf(startTime);
        this.endTime = String.valueOf(endTime);
        this.reservationDate = String.valueOf(reservationDate);
        this.userId = userId;
        this.deport = deport;
        this.sportGroundId = sportGroundId;
        this.pending = pending;
    }

    private int id;

    private String createDate;

    private String startTime ;

    private String endTime;

    private String reservationDate;

    private int userId;

    private String deport;

    private int sportGroundId;

    private int pending;
}
