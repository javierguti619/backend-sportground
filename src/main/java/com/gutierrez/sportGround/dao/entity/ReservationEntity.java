package com.gutierrez.sportGround.dao.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Table(name="reservation")
@Entity
@Data
public class ReservationEntity implements Serializable {

    @Id
    @GeneratedValue
    @Column(name="reser_id")
    private Integer id;

    @Column(name="create_date")
    private LocalDate createDate;

    @Column(name="start_time ")
    private LocalDateTime startTime ;


    @Column(name="end_time")
    private LocalDateTime endTime;

    @Column(name = "reservation_date")
    private LocalDate reservationDate;

    @Column(name= "user_id")
    private int userId;

    @Column(name= "deport_type")
    private String deport;

    @Column(name= "sport_ground_id")
    private int sportGroundId;

    @Column(name= "pending")
    private int pending;

}
