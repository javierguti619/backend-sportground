package com.gutierrez.sportGround.mail;

import com.gutierrez.sportGround.configuration.EmailConfiguration;
import lombok.RequiredArgsConstructor;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Component;

import java.util.Map;

@RequiredArgsConstructor
@Component
public class EmailService {

    private final EmailConfiguration emailConfiguration;
    private final MailContentBuilder mailContentBuilder;

    public void prepareAndSend(String email, String subject, Map<String, String> map, EmailType emailType) {

        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost(emailConfiguration.getHost());
        mailSender.setPort(emailConfiguration.getPort());
        mailSender.setUsername(emailConfiguration.getUsername());
        mailSender.setPassword(emailConfiguration.getPassword());

        MimeMessagePreparator messagePreparator = mimeMessage -> {
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage);
            helper.setFrom(emailConfiguration.getEmail());
            helper.setTo(email);
            helper.setSubject(subject);
            helper.setText(mailContentBuilder.buildReservationMail(map, emailType.getTemplateName()), true);

        };
        // Send mail
        mailSender.send(messagePreparator);

    }

}
