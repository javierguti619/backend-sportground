package com.gutierrez.sportGround.dao.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDateTime;

@Table(name="event")
@Entity
public class EventEntity implements Serializable {

    public EventEntity() {}

    public EventEntity(int eventId,
                       int venueId,
                       String name,
                       int groundId,
                       LocalDateTime dateTime,
                       int status) {
        this.eventId = eventId;
        this.venueId = venueId;
        this.name = name;
        this.groundId = groundId;
        this.dateTime = dateTime;
        this.status = status;
    }

    @Id
    @Column(name = "idevent")
    private int eventId;

    @Column(name = "venueId")
    private int venueId;

    @Column(name = "name")
    private String name;


    @Column(name = "groundId")
    private int groundId;

    @Column(name = "dateTime")
    private LocalDateTime dateTime;

    @Column(name = "status")
    private int status;


    public int getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    public int getVenueId() {
        return venueId;
    }

    public void setVenueId(int venueId) {
        this.venueId = venueId;
    }

    public int getGroundId() {
        return groundId;
    }

    public void setGroundId(int groundId) {
        this.groundId = groundId;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
