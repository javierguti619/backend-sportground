package com.gutierrez.sportGround.controller;

import com.gutierrez.sportGround.dao.entity.EventEntity;
import com.gutierrez.sportGround.model.MEvent;
import com.gutierrez.sportGround.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST, RequestMethod.PUT})
@RequestMapping("/event")
public class EventController {

    @Autowired
    @Qualifier("eventService")
    private EventService eventService;

    @PostMapping("/add")
    public boolean addEvent(@RequestBody @Valid EventEntity event) {

        return eventService.create(event);
    }

    @GetMapping("/all")
    public List<MEvent> getAllEvents() {
        return eventService.getsAll();
    }
}
