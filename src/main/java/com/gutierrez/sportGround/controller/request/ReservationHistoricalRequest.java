package com.gutierrez.sportGround.controller.request;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.time.LocalDate;

@Data
@RequiredArgsConstructor
public class ReservationHistoricalRequest {
    private Long venueId;
    private LocalDate startDate;
    private LocalDate endDate;
}


