package com.gutierrez.sportGround.converter;

import com.gutierrez.sportGround.dao.entity.VenueEntity;
import com.gutierrez.sportGround.model.MVenue;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component("venueConverter")
public class VenueConverter {

    public List<MVenue> converList(List<VenueEntity> venues) {
        List<MVenue> mVenues = new ArrayList<>();

        for( VenueEntity venue: venues ) {
            mVenues.add(new MVenue(venue));
        }
        return mVenues;
    }
}
