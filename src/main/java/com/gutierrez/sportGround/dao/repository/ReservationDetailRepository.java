package com.gutierrez.sportGround.dao.repository;

import com.gutierrez.sportGround.dao.entity.ReservationDetailEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.io.Serializable;
import java.util.List;

public interface ReservationDetailRepository extends JpaRepository<ReservationDetailEntity, Serializable> {

    @Query(value="select r.reser_id ,r.create_date, r.start_time, r.end_time, r.reservation_date, s.description, " +
            " s.image, s.type , v.venue_name, v.address, v.phone " +
            "from  reservation r " +
            "left join ground s on r.sport_ground_id = s.ground_id " +
            "left join venue v  on s.venue_id = v.venue_id " +
            "where r.user_id =:userId and r.pending=1;", nativeQuery = true)
    List<ReservationDetailEntity>  findByUser(@Param("userId") int userId);
}
