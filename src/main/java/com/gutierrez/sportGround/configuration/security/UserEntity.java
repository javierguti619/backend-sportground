
package com.gutierrez.sportGround.configuration.security;

import lombok.Data;

@Data
public class UserEntity {

    private String email;
    private String password;

}

