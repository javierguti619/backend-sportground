package com.gutierrez.sportGround.configuration.security;


import com.google.common.net.HttpHeaders;
import com.gutierrez.sportGround.dao.entity.UserEntity;
import com.gutierrez.sportGround.service.UserService;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.AllArgsConstructor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.StringUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.stream.Collectors;

import static java.util.Collections.emptyList;

@AllArgsConstructor
public class JwtUtil {

    // Método para crear el JWT y enviarlo al cliente en el header de la respuesta
    static void addAuthentication(HttpServletResponse res, Authentication auth) {


        String token = Jwts.builder()
                .setSubject(auth.getName())
                // Hash con el que firmaremos la clave

                .signWith(SignatureAlgorithm.HS512, "P@tit0")
                .compact();

        //agregamos al encabezado el token
        res.addHeader("Authorization", "Bearer " + token);
        res.setContentType("application/json");
        res.setCharacterEncoding("utf-8");

        PrintWriter out = null;
        try {
            out = res.getWriter();

            JSONObject json = new JSONObject();
            json.put("token", token);
            json.put("userId", auth.getName());
            json.put("userType", auth.getAuthorities().stream().collect(Collectors.toList()).get(0));

            // finally output the json string
            out.print(json.toString());

        } catch (IOException e) {
            e.printStackTrace();
        }

        res.addCookie(new Cookie("token", token));
    }

    // Método para validar el token enviado por el cliente
    static Authentication getAuthentication(HttpServletRequest request) {

        String headerPrefix = "Bearer ";
        String tokenValue = null;

        String bearerToken = request.getHeader(HttpHeaders.AUTHORIZATION);

        if (StringUtils.hasText(bearerToken) && bearerToken.startsWith(headerPrefix)) {
            tokenValue = bearerToken.substring(headerPrefix.length(), bearerToken.length());
        }

        // si hay un token presente, entonces lo validamos
        if (tokenValue != null) {
            String user = Jwts.parser()
                    .setSigningKey("P@tit0")
                    .parseClaimsJws(tokenValue) //este metodo es el que valida
                    .getBody()
                    .getSubject();

            // Recordamos que para las demás peticiones que no sean /login
            // no requerimos una autenticacion por username/password
            // por este motivo podemos devolver un UsernamePasswordAuthenticationToken sin password
            return user != null ?
                    new UsernamePasswordAuthenticationToken(user, null, emptyList()) :
                    null;
        }
        return null;
    }

}

