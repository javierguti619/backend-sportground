package com.gutierrez.sportGround.service;

import com.gutierrez.sportGround.converter.EventConverter;
import com.gutierrez.sportGround.dao.entity.EventEntity;
import com.gutierrez.sportGround.model.MEvent;
import com.gutierrez.sportGround.dao.repository.EventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("eventService")
public class EventService {

    @Autowired
    @Qualifier("eventRepository")
    private EventRepository eventRepository;

    @Autowired
    @Qualifier("eventConverter")
    private EventConverter eventConverter;

    public boolean create(EventEntity event) {

        try {
            eventRepository.save(event);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean upDate(EventEntity event) {

        try {
            eventRepository.save(event);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean delete( int id) {

        try {

            EventEntity event = eventRepository.findByEventId(id);
            eventRepository.delete((event));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public List<MEvent> getsAll() {
        return  eventConverter.converList(eventRepository.findAll());
    }

    public MEvent getsEvent( int id) {
        return new MEvent((eventRepository.findByEventId(id)));
    }

}
