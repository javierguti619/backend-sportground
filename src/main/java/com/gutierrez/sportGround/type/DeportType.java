package com.gutierrez.sportGround.type;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import com.google.common.collect.Maps;
import com.google.common.collect.ImmutableMap;

import java.util.Arrays;

@Getter
@RequiredArgsConstructor
public enum DeportType {
    VOLLEYBALL("VO", "Voley"),
    BASKETBALL("BA", "Básquet"),
    FOOTBALL("FU","Fútbol");

    private static ImmutableMap<String, DeportType> reverseLookupCode = Maps.uniqueIndex(Arrays.asList(DeportType.values()), DeportType::getCode);
    private final String code;
    private final String description;


    public static DeportType fromCode(String code) {
        DeportType deportType = reverseLookupCode.get(code);
        if (deportType != null) {
            return deportType;
        }
        throw new IllegalArgumentException("Invalid deport type");
    }
}
