package com.gutierrez.sportGround.mail;

        import com.google.common.collect.ImmutableMap;
        import com.google.common.collect.Maps;
        import lombok.Getter;
        import lombok.RequiredArgsConstructor;

        import java.util.Arrays;

@Getter
@RequiredArgsConstructor
public enum EmailType {
    REGISTER("1", "registerMail"),
    RESERVATION("2", "reservationMail");

    private final String code;
    private final String templateName;

    private static ImmutableMap<String, EmailType> reverseLookupCode = Maps.uniqueIndex(Arrays.asList(EmailType.values()), EmailType::getCode);

}