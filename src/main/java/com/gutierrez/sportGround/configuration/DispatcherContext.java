package com.gutierrez.sportGround.configuration;

import com.gutierrez.sportGround.controller.UserController;
import com.gutierrez.sportGround.controller.resolver.UserResolver;
import com.gutierrez.sportGround.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import java.util.List;

@Configuration
@RequiredArgsConstructor
public class DispatcherContext extends WebMvcConfigurationSupport {

    private final UserService userService;

    @Override
    protected void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        argumentResolvers.add(new UserResolver(userService));
    }
}