package com.gutierrez.sportGround.controller.request;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;

@Data
@AllArgsConstructor
public class ReservationRequest {
    private int sportGroundId;
    private LocalDate reservationDate;
}
