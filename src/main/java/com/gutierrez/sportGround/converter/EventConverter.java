package com.gutierrez.sportGround.converter;

import com.gutierrez.sportGround.dao.entity.EventEntity;
import com.gutierrez.sportGround.model.MEvent;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component("eventConverter")
public class EventConverter {

    public List<MEvent> converList(List<EventEntity> events) {

        List<MEvent> mEvents = new ArrayList<>();

        for(EventEntity event: events) {
            mEvents.add( new MEvent(event));
        }
        return mEvents;
    }
}
